class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets do |t|
      t.string :number, null: false
      t.string :flight_key, null: false

      t.timestamps
    end
  end
end
