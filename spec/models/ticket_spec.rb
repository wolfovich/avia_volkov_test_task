require 'rails_helper'

describe Ticket, type: :model do
  describe 'validations' do
    let(:ticket) { FactoryBot.build(:ticket) }

    it 'valid with number and flight_key' do
      expect(ticket).to be_valid
    end

    it 'invalid with a duplicate number and flight_key' do
      ticket.save
      ticket2 = FactoryBot.build(:ticket)
      ticket2.valid?
      expect(ticket2.errors[:number]).to include('has already been taken')
    end
  end
end
