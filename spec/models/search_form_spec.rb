require 'rails_helper'

describe SearchForm, type: :model do
  describe 'validations' do
    let(:search_form) { SearchForm.new }

    it 'invalid if empty' do
      expect(search_form).not_to be_valid
    end

    it 'invalid with two empty flight_routes' do
      search_form.build_search_routes(2)
      expect(search_form).not_to be_valid
    end
  end

  describe 'search flights' do
    context 'one route' do
      let(:search_route) { FactoryBot.build(:search_route) }
      let(:search_form) { SearchForm.new(search_routes: [search_route]) }

      before do
        search_form.save
      end

      it 'contain at least one flight' do
        expect(search_form.flights.count).to be > 0
      end

      it 'first flight contain one flight_route' do
        expect(search_form.flights.first.flight_routes.count).to be > 0
      end
    end

    context 'two routes' do
      let(:search_route1) { FactoryBot.build(:search_route) }
      let(:search_route2) do
        FactoryBot.build(:search_route,
                         departure: 'SIP',
                         arrival: 'MOW',
                         date: '31-01-2020')
      end
      let(:search_form) { SearchForm.new(search_routes: [search_route1, search_route2]) }

      before do
        search_form.save
      end

      it 'contain at least one flight' do
        expect(search_form.flights.count).to be > 0
      end

      it 'first flight contain two flight_routes' do
        expect(search_form.flights.first.flight_routes.count).to eq 2
      end
    end
  end
end
