require 'rails_helper'

describe TicketForm, type: :model do
  describe 'validations' do
    let(:ticket_form) { TicketForm.new }

    it 'invalid if empty' do
      expect(ticket_form).not_to be_valid
    end

    it 'valid with flight' do
      ticket_form.flight = Flight.new(flight_routes: [FlightRoute.new])
      ticket_form.flight_uuid = ticket_form.flight.uuid
      expect(ticket_form).to be_valid
    end
  end
end