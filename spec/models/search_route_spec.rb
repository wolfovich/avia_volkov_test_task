require 'rails_helper'

describe SearchRoute, type: :model do
  describe 'validations' do
    let(:search_route) { SearchRoute.new }

    it 'invalid if empty and search_route_id eq 0' do
      expect(search_route).not_to be_valid
    end

    it 'valid if empty and search_route_id not eq 0' do
      search_route.search_route_id = 1
      expect(search_route).to be_valid
    end

    it 'date should be in DD-MM-YYYY format' do
      search_route.date = '22.11.1111'
      search_route.valid?
      expect(search_route.errors[:date]).to include('format should be DD-MM-YYYY')
    end

    it 'invalid with date in the past' do
      search_route.date = '22-11-1111'
      search_route.valid?
      expect(search_route.errors[:date]).to include("can't be in the past")
    end

    it "invalid if all fields aren't filled in" do
      result = SearchRoute.new(date: '22-11-9999', departure: nil, arrival: nil, search_route_id: 1).valid? ||
               SearchRoute.new(date: '22-11-9999', departure: 'MOW', arrival: nil, search_route_id: 1).valid? ||
               SearchRoute.new(date: '22-11-9999', departure: nil, arrival: 'SIP', search_route_id: 1).valid? ||
               SearchRoute.new(date: nil, departure: nil, arrival: 'SIP', search_route_id: 1).valid? ||
               SearchRoute.new(date: nil, departure: 'MOW', arrival: 'SIP', search_route_id: 1).valid? ||
               SearchRoute.new(date: nil, departure: 'MOW', arrival: nil, search_route_id: 1).valid?
      expect(result).to eq(false)
    end

    it 'valid with all fields filled in with date not in past' do
      search_route = SearchRoute.new(date: '22-11-9999', departure: 'MOW', arrival: 'SIP')
      expect(search_route).to be_valid
    end
  end
end
