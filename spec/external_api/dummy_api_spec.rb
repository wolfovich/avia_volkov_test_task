require 'rails_helper'

describe DummyApi::Client do
  let(:dummy_api_client) { DummyApi::Client.new(base_uri: Rails.application.credentials.dig(Rails.env.to_sym, :dummy_api_host)) }
  let(:dummy_api_client_with_cache) { DummyApi::Client.new(base_uri: Rails.application.credentials.dig(Rails.env.to_sym, :dummy_api_host), cache: true) }
  let(:one_route_options) do
    { destinations:
         {
           '0' => { departure: 'MOW', arrival: 'SIP', date: '22-11-2019' }
         } }
  end
  let(:two_routes_options) do
    { destinations: {
      '0' => { departure: 'MOW', arrival: 'SIP', date: '22-11-2019' },
      '1' => { departure: 'SIP', arrival: 'MOW', date: '31-01-2020' }
    } }
  end

  describe '#search' do
    let(:memory_store) { ActiveSupport::Cache.lookup_store(:memory_store) }
    let(:cache) { Rails.cache }
    context 'empty requests' do
      before do
        @response = dummy_api_client.search({})
      end

      it 'contain at least one warning' do
        expect(@response.warnings.count).to be > 0
      end

      it "first warning message eq 'No appropriate flights found on the segment 1'" do
        expect(@response.warnings.first.text).to eq('No appropriate flights found on the segment 1')
      end
    end

    describe 'valid requests' do
      context 'one route' do
        before do
          @response = dummy_api_client.search(one_route_options)
        end

        it 'does not contain any  warnings' do
          expect(@response.warnings.count).to be(0)
        end

        it 'contain at least one variant' do
          expect(@response.variants.count).to be > 0
        end

        it 'contain variant with one flight' do
          expect(@response.variants.first.flights.count).to be 1
        end

        context 'with cache enabled' do
          before do
            allow(Rails).to receive(:cache).and_return(memory_store)
            Rails.cache.clear
          end

          it 'response with cache: true eq to response with cache: false' do
            dummy_api_client_with_cache.search(two_routes_options)
            @response2 = dummy_api_client_with_cache.search(one_route_options)
            expect(@response.to_json).to eql(@response2.to_json)
          end
        end
      end

      context 'two routes' do
        before do
          @response = dummy_api_client.search(two_routes_options)
        end

        it 'does not contain any warnings' do
          expect(@response.warnings.count).to be 0
        end

        it 'contain at least one variant' do
          expect(@response.variants.count).to be > 0
        end

        it 'contain variant with two flights' do
          expect(@response.variants.first.flights.count).to be 2
        end

        it 'flight have company' do
          flight = @response.variants.first.flights.first
          expect(flight.company.text).not_to be_empty
        end

        context 'with cache enabled' do
          before do
            allow(Rails).to receive(:cache).and_return(memory_store)
            Rails.cache.clear
          end

          it 'response with cache: true eq to response with cache: false' do
            dummy_api_client_with_cache.search(one_route_options)
            @response2 = dummy_api_client_with_cache.search(two_routes_options)
            expect(@response.to_json).to eql(@response2.to_json)
          end
        end
      end
    end
  end

  describe '#booking' do
    context 'empty requests' do
      before do
        @response = dummy_api_client.booking({})
      end

      it 'contain at least one warning' do
        expect(@response.warnings.count).to be > 0
      end

      it "first warning message eq 'Booking creation error'" do
        expect(@response.warnings.first.text).to eq('Booking creation error')
      end
    end

    describe 'valid requests' do
      context 'one route' do
        before do
          @response = dummy_api_client.booking(one_route_options)
        end

        it 'does not contain any warnings' do
          expect(@response.warnings.count).to be(0)
        end

        it 'contain at least one ticket' do
          expect(@response.tickets.count).to be > 0
        end
      end

      context 'two routes' do
        before do
          @response = dummy_api_client.booking(two_routes_options)
        end

        it 'does not contain any warnings' do
          expect(@response.warnings.count).to be 0
        end

        it 'contain two tickets' do
          expect(@response.tickets.count).to be > 0
        end

        it 'ticket have number' do
          ticket = @response.tickets.first
          expect(ticket.number.text).not_to be_empty
        end
      end
    end
  end
end
