FactoryBot.define do
  factory :search_route do
    date { '22-11-2019' }
    departure { 'MOW' }
    arrival { 'SIP' }
    sequence(:search_route_id) { |n| n }
  end
end