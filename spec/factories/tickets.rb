FactoryBot.define do
  factory :ticket do
    number { '1111' }
    flight_key { 'SIPMOW' }
  end
end
