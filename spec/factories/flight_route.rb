FactoryBot.define do
  factory :flight_route do
    association :avia_search
    date { '22-11-2019' }
    departure { 'MOW' }
    arrival { 'SIP' }
    flight_key { '' }
  end
end
