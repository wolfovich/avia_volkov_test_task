class SearchFormsController < ApplicationController
  def show
    build_resource
  end

  def create
    build_resource
    if @resource.save
      search_result = Gds::GdsSearchService.new.search_flights(@resource)
      # TODO: add errors from search_result.errors to form object
      @flights = search_result.flights
    end
    render 'show'
  end

  private

  def build_resource
    @resource = SearchForm.new(resource_params)
    @resource.build_search_routes(2)
    @resource.search_routes.map! { |search_route| FrontSearchRouteValidation.new(search_route) }
  end

  def resource_params
    resource_params = params[:search_form]
    resource_params = if resource_params
                        resource_params.permit(search_routes_attributes: %i[departure date arrival search_route_id]) # TODO: check allowed params in Policy object (Pundit etc)
                      else
                        {}
                      end
    # ActiveType::Object#nests_many does not support :limit option :(
    resource_params[:search_routes_attributes]&.slice!('0', '1')
    resource_params
  end
end