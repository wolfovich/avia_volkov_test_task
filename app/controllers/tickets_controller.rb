class TicketsController < ApplicationController
  def index
    @resources = Ticket.all
  end

  def destroy_all
    Ticket.destroy_all
    redirect_to search_path
  end
end