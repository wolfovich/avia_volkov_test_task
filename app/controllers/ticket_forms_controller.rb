class TicketFormsController < ApplicationController
  def create
    build_resource
    @resource.save
  end

  private

  def build_resource
    @resource = TicketForm.new(flight_uuid: params[:flight_uuid])
  end
end