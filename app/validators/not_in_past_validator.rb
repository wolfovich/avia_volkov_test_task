class NotInPastValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    (Date.parse(value) <= Time.zone.today) && record.errors.add(attribute, "can't be in the past")
  end
end