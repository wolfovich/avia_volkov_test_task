module Tickets
  class Dummy < Ticket
    def self.build(ticket:, flight:)
      new(flight_key: flight.flight_key, number: ticket.number.text)
    end
  end
end