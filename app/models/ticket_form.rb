class TicketForm < ActiveType::Object
  attribute :flight
  attribute :flight_uuid
  attr_reader :tickets

  validates :flight_uuid, presence: true
  validates :flight, presence: true
  after_initialize :load_flight
  after_save :booking_and_save_tikets

  private

  def load_flight
    self.flight = Rails.cache.read(flight_uuid)
  end

  def booking_and_save_tikets
    booking_tikets
    save_tickets
  end

  def booking_tikets
    booking_service = Gds::GdsBookingServiceFactory.create(gds_id: flight.gds_id)
    booking_result = booking_service.booking_tickets(Gds::TicketFormDecorator.new(self))
    booking_result.errors.each do |error|
      error.each do |key,value|
        errors.add(key, value)
      end
    end
    @tickets = booking_result.tickets
    @tickets.each { |ticket| ticket.flight_key = flight.flight_key}
  end

  def save_tickets
    ActiveRecord::Base.transaction do
      if Ticket.flight_was_purchased?(flight)
        errors.add(:ticket_form, "The ticket's already been bought.")
      else
        @tickets.map(&:save)
      end
    end
  end
end
