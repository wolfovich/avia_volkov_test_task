class FlightRoute
  attr_reader :air_company,
              :arrival,
              :departure,
              :flight_number,
              :flight_time,
              :departure_date,
              :departure_time,
              :arrival_date,
              :arrival_time,
              :iSegmentDest,
              :iSegmentOrig
  def initialize(departure_date:,
                 departure_time:,
                 departure:,
                 arrival_date:,
                 arrival_time:,
                 arrival:,
                 air_company:,
                 flight_number:,
                 flight_time:,
                 iSegmentOrig:,
                 iSegmentDest:
                 )
    @departure_date = departure_date
    @departure_time = departure_time
    @departure = departure
    @arrival_date = arrival_date
    @arrival_time = arrival_time
    @arrival = arrival
    @air_company = air_company
    @flight_number = flight_number
    @flight_time = flight_time
    @iSegmentOrig = iSegmentOrig
    @iSegmentDest = iSegmentDest
  end

  def departure_full
    [departure, departure_date, departure_time].join(' ')
  end

  def arrival_full
    [arrival, arrival_date, arrival_time].join(' ')
  end

  def flight_route_key
    [flight_number, departure, arrival, date].join
  end

  def blank?
    date.blank? && departure.blank? && arrival.blank?
  end

  def present?
    date.present? || departure.present? || arrival.present?
  end

  # methods for build new api request...
  def date
    departure_date
  end
end