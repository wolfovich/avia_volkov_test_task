class Ticket < ApplicationRecord
  validates :number, :flight_key, presence: true
  validates :number, uniqueness: { scope: [:flight_key] }

  def self.flight_was_purchased?(flight)
    where(flight_key: flight.flight_key).exists?
  end
end
