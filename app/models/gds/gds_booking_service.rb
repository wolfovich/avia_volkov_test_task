module Gds
  class GdsBookingService
    def initialize(adapter: gds_booking_adapter)
      @gds_boking_adapter = adapter # TODO: check if adapter.superclass is_a? BookingAdapterInterface and raise error if not
    end

    def booking_tickets(request_form)
      booking_result = @gds_boking_adapter.booking(request_form)
      OpenStruct.new(tickets: booking_result.tickets, errors: booking_result.errors)
    end
  end
end