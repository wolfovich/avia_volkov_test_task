module Gds
  class GdsSearchService
    def initialize
      @search_adpaters = [Gds::Dummy::DummySearchAdapterFactory.create]
    end

    def search_flights(request_form)
      errors = []
      flights = []
      @search_adpaters.each do |adapter|
        search_result = adapter.search(request_form) # TODO: check if adapter.superclass is_a? SearchAdapterInterface and raise error if not
        errors += search_result.errors
        flights += search_result.flights
      end
      OpenStruct.new(errors: errors, flights: flights)
    end

    def booking_tickets
      @gds_providers.each do |provider|
        provider.booking_tickets
        @errors += provider.errors
        @tickets += provider.tickets
      end
      self
    end
  end
end