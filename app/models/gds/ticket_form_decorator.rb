module Gds
  class TicketFormDecorator < SimpleDelegator
    def presented_search_routes
      flight.flight_routes.reject(&:blank?)
    end

    def flight
      super
    end
  end
end