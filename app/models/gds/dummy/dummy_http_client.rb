module Gds
  module Dummy
    class DummyHttpClient
      include HTTParty
      include HttpStatusCodes
      include Exceptions

      def initialize(base_uri: '', cache: false)
        self.class.base_uri base_uri
        @cache = cache
        @cacher = DummyCache
      end

      def search(options = {})
        request(
          http_method: :get,
          endpoint: '/avia/search?' + options_to_query(options),
          cache: @cache
        )
      end

      def booking(options = {})
        request(
          http_method: :post,
          endpoint: '/avia/booking?' + options_to_query(options)
        )
      end

      def request(http_method:, endpoint:, cache: false)
        @response = if cache
                      @cacher.fetch(key: cache_key(http_method: http_method, endpoint: endpoint)) do
                        self.class.public_send(http_method, endpoint)
                      end
                    else
                      self.class.public_send(http_method, endpoint)
                    end
        return @response.body if response_successful?

        raise error_class, "Code: #{@response.code}, response: #{@response.body}"
      end

      def options_to_query(options)
        CGI.unescape(options.to_query)
      end

      def error_class
        case @response.code
        when HTTP_BAD_REQUEST_CODE
          BadRequestError
        when HTTP_NOT_FOUND_CODE
          NotFoundError
        else
          ApiError
        end
      end

      def response_successful?
        @response.code == HTTP_OK_CODE
      end

      def cache_key(http_method:, endpoint:)
        http_method.to_s + Digest::MD5.hexdigest(endpoint)
      end
    end
  end
end
