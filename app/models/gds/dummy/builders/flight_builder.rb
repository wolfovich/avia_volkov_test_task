module Gds
  module Dummy
    module Builders
      class FlightBuilder
        def initialize
          @flight_route_builder = FlightRouteBuilder.new
        end

        def build(variant)
          flight_routes = variant.locate('flight').map do |flight|
            @flight_route_builder.build(flight)
          end
          Flight.new(price: variant.variant_total.text,
                     flight_routes: flight_routes,
                     gds_id: Gds::GdsReferences::GDS_DUMMY_ID)
        end
      end
    end
  end
end