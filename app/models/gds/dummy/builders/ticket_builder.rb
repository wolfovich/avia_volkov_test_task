module Gds
  module Dummy
    module Builders
      class TicketBuilder
        def build(ticket)
          Ticket.new(number: ticket.number.text)
        end
      end
    end
  end
end