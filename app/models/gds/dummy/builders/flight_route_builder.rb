module Gds
  module Dummy
    module Builders
      class FlightRouteBuilder
        def build(flight)
          departure_date = Date.strptime(flight.deptdate.text, '%d.%m.%y').strftime('%d-%m-%Y')
          arrival_date = Date.strptime(flight.arrvdate.text, '%d.%m.%y').strftime('%d-%m-%Y')
          FlightRoute.new(
            arrival_time: flight.arrvtime.text,
            arrival_date: arrival_date,
            arrival: flight.destination.text,
            departure_date: departure_date,
            departure_time: flight.depttime.text,
            departure: flight.origin.text,
            flight_time: flight.flightTime.text,
            flight_number: flight.num.text,
            air_company: flight.company.text,
            iSegmentDest: flight.attributes['iSegmentDest'.to_sym],
            iSegmentOrig: flight.attributes['iSegmentOrig'.to_sym]
          )
        end
      end
    end
  end
end