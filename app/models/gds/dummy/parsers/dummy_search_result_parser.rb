module Gds
  module Dummy
    module Parsers
      class DummySearchResultParser
        def parse(xml:)
          doc = Ox.parse(xml)
          warnings = doc.gds.answer.pricing.info.locate('warning').map { |warning| { dummy_search: warning.text } } rescue []  # TODO: remove rescue
          variants = doc.gds.answer.pricing.locate('variant')
          OpenStruct.new(variants: variants, warnings: warnings)
        end
      end
    end
  end
end