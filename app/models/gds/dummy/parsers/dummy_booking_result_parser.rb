module Gds
  module Dummy
    module Parsers
      class DummyBookingResultParser
        def parse(xml:)
          doc = Ox.parse(xml)
          warnings = doc.gds.answer.booking.info.locate('warning').map { |warning| { dummy_search: warning.text } } rescue [] # TODO: remove rescue
          tickets = doc.gds.answer.booking.locate('ticket')
          OpenStruct.new(tickets: tickets, warnings: warnings)
        end
      end
    end
  end
end