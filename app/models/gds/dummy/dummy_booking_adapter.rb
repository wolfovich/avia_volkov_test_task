module Gds
  module Dummy
    class DummyBookingAdapter < SearchAdapterInterface
      def initialize(api_client:, parser:, builder:)
        @builder = builder
        @parser = parser
        @api_client = api_client
      end

      def booking(booking_request)
        options = build_booking_options(booking_request)
        raw_result = @api_client.booking(options)
        parse_result = @parser.parse(xml: raw_result)
        tickets = parse_result.tickets.map { |ticket| @builder.build(ticket) }
        OpenStruct.new(tickets: tickets, errors: parse_result.warnings)
      end

      private

      def build_booking_options(booking_request)
        destinations = {}
        booking_request.presented_search_routes.each_with_index do |route, index|
          destinations[index.to_s] = { departure: route.iSegmentOrig, arrival: route.iSegmentDest, date: route.date }
        end
        { destinations: destinations }
      end
    end
  end
end