module Gds
  module Dummy
    class DummySearchAdapterFactory
      def self.create()
        api_client = DummyHttpClient
                      .new(base_uri: Rails.application.credentials.dig(Rails.env.to_sym, :gds, :dummy, :host),
                           cache: true)
        parser = Parsers::DummySearchResultParser.new
        flight_builder = Builders::FlightBuilder.new
        DummySearchAdapter.new(api_client: api_client, parser: parser, builder: flight_builder)
      end
    end
  end
end