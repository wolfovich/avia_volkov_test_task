module Gds
  module Dummy
    module Exceptions
      ExceptionError = Class.new(StandardError)
      BadRequestError = Class.new(ExceptionError)
      NotFoundError = Class.new(ExceptionError)
      ApiError = Class.new(ExceptionError)
    end
  end
end