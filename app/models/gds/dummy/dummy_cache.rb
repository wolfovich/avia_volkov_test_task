module Gds
  module Dummy
    class DummyCache
      def self.fetch(key:)
        Rails.cache.fetch(key, expires_in: 5.minutes) do
          yield
        end
      end
    end
  end
end