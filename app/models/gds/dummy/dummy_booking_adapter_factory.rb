module Gds
  module Dummy
    class DummyBookingAdapterFactory
      def self.create
        base_uri = Rails.application.credentials.dig(Rails.env.to_sym,
                                                     :gds,
                                                     :dummy,
                                                     :host)
        api_client = DummyHttpClient.new(base_uri: base_uri, cache: false)
        parser = Parsers::DummyBookingResultParser.new
        ticket_builder = Builders::TicketBuilder.new
        DummyBookingAdapter.new(api_client: api_client, parser: parser, builder: ticket_builder)
      end
    end
  end
end