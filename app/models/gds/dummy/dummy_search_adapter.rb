module Gds
  module Dummy
    class DummySearchAdapter < SearchAdapterInterface
      def initialize(api_client:, parser:, builder:)
        @builder = builder
        @parser = parser
        @api_client = api_client
      end

      def search(search_request)
        options = build_search_options(search_request)
        raw_result = @api_client.search(options)
        parse_result = @parser.parse(xml: raw_result)
        flights = parse_result.variants.map { |variant| @builder.build(variant) }
        flights.reject! { |flight| Ticket.flight_was_purchased?(flight) } # TODO: is it right place for filtering flights?
        OpenStruct.new(flights: flights, errors: parse_result.warnings)
      end

      private

      def build_search_options(search_request)
        destinations = {}
        search_request.presented_search_routes.each_with_index do |route, index|
          destinations[index.to_s] = { departure: route.departure, arrival: route.arrival, date: route.date }
        end
        { destinations: destinations }
      end

      def build_booking_options(search_request)
        destinations = {}
        search_request.presented_search_routes.each_with_index do |route, index|
          destinations[index.to_s] = { departure: route.departure, arrival: route.arrival, date: route.date }
        end
        { destinations: destinations }
      end
    end
  end
end