module Gds
  class BookingAdapterInterface
    def booking(_booking_request)
      raise NotImplementedError
    end
  end
end