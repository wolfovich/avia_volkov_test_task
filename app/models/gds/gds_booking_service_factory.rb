module Gds
  class GdsBookingServiceFactory
    def self.create(gds_id:)
      adapter = case gds_id
                when Gds::GdsReferences::GDS_DUMMY_ID
                  Dummy::DummyBookingAdapterFactory.create
                else
                  raise NotImplementedError
                end
      Gds::GdsBookingService.new(adapter: adapter)
    end
  end
end