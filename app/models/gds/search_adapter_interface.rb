module Gds
  class SearchAdapterInterface
    def search(_search_request)
      raise NotImplementedError
    end
  end
end