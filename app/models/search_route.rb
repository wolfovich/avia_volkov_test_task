class SearchRoute < ActiveType::Object
  attribute :search_route_id, :integer, default: 0
  attribute :date
  attribute :departure
  attribute :arrival

  def present?
    date.present? || departure.present? || arrival.present?
  end

  def blank?
    date.blank? && departure.blank? && arrival.blank?
  end
end