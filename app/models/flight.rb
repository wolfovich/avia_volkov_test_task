class Flight
  attr_reader :flight_routes,
              :price,
              :uuid,
              :gds_id

  def initialize(flight_routes:, price:, gds_id:)
    @price = price
    raise StandardError unless flight_routes.is_a?(Array) && flight_routes.present?
    @flight_routes = flight_routes
    @gds_id = gds_id
    @uuid = SecureRandom.uuid
    Rails.cache.write(uuid, self)
  end

  def flight_key
    flight_routes.map(&:flight_route_key).join('')
  end
end