class SearchForm < ActiveType::Object
  nests_many :search_routes
  attr_reader :flights

  validates :search_routes, presence: true

  def build_search_routes(num)
    return self unless num.is_a?(Integer) && num > 1

    self.search_routes = [] if self.search_routes.nil?
    search_routes_count = search_routes.count
    return self if search_routes_count >= num

    (num - search_routes_count).times { |i| self.search_routes << SearchRoute.new(search_route_id: search_routes_count + i) }
    self
  end

  def presented_search_routes
    search_routes.reject(&:blank?)
  end
end