class FrontSearchRouteValidation < SimpleDelegator
  include ActiveModel::Validations

  with_options if: :should_validate? do
    validates :date, :departure, :arrival, presence: true
    validates :date, format: { with: /(^\d{2}-\d{2}-\d{4}$)/, message: 'format should be DD-MM-YYYY' }
  end
  validates :date, not_in_past: true, if: proc { |obj| obj.errors.messages[:date].empty? && should_validate? }

  def to_model; self; end

  private

  def should_validate?
    search_route_id.zero? || present?
  end
end