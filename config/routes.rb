Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'search_forms#show'
  post '/', to: 'search_forms#create', as: 'search'
  # resource :avia_search, only: [:show, :create]

  resource :ticket_form, only: [:create]
  resources :tickets, only: [:index] do
    get :destroy_all, on: :collection
  end
end
